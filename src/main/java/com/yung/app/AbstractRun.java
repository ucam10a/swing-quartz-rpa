package com.yung.app;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.yung.app.RunObject;

public abstract class AbstractRun implements RunObject {

    public static boolean STOP = false;
    private static final String NEW_LINE = System.getProperty("line.separator");
    
    public static boolean isSTOP() {
        return STOP;
    }

    public static void setSTOP(boolean sTOP) {
        STOP = sTOP;
    }
    
    public static void logMessage(JScrollPane scrollPane, JTextArea log, String message) {
        if (STOP) throw new RuntimeException("Cancel process");
        JScrollBar vertical = scrollPane.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
        log.append(message + NEW_LINE);
        System.out.println(message);
    }
    
}
