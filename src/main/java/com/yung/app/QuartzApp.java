package com.yung.app;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.AbstractTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import com.yung.app.job.AbstractJob;
import com.yung.app.job.RPAJob;
import com.yung.tool.AppLogger;

/**
 * A Quartz app to run job
 * 
 * @author Yung Long Li
 *
 */
public class QuartzApp {

    private static final AppLogger logger = AppLogger.getLogger(QuartzApp.class);

    /**
     * run quartz job
     * @param prop 
     * @param log 
     * @param logScrollPane 
     */
    public void runQuartz(Properties prop, JScrollPane logScrollPane, JTextArea log) {

        Scheduler scheduler = null;
        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            
            // define the job and tie it to our HelloJob class
            JobDetail rpa = new JobDetailImpl();
            ((JobDetailImpl) rpa).setName("rpa");
            ((JobDetailImpl) rpa).setGroup("group1");
            ((JobDetailImpl) rpa).setJobClass(RPAJob.class);
            rpa.getJobDataMap().put("prop", prop);
            rpa.getJobDataMap().put("logScrollPane", logScrollPane);
            rpa.getJobDataMap().put("log", log);
            
            AbstractTrigger<?> trigger = null;
            String cronExpress = prop.getProperty("rpa.cron");
            if (cronExpress != null && !"".equals(cronExpress)) {
                logger.info("cronExpress: " + cronExpress);
                AbstractJob.logMessage(logScrollPane, log, "cronExpress: " + cronExpress);
                trigger = new CronTriggerImpl();
                trigger.setName("trigger");
                trigger.setGroup("group");
                trigger.setJobKey(rpa.getKey());
                ((CronTriggerImpl) trigger).setCronExpression(cronExpress);
            } else {
                long interval = 24 * 60 * 60 * 1000L; // one day
                String seconds = prop.getProperty("rpa.repeat.second");
                if (seconds != null && !"".equals(seconds) && AbstractJob.isNumeric(seconds)) {
                    interval = new BigDecimal(seconds).longValue() * 1000L;
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                    String msg = "quartz use default schedule: run on every day at " + sdf.format(new Date());
                    logger.info(msg);
                    AbstractJob.logMessage(logScrollPane, log, msg);
                }
                logger.info("interval: " + interval);
                AbstractJob.logMessage(logScrollPane, log, "interval: " + interval);
                Date now = new Date();
                trigger = new SimpleTriggerImpl();
                trigger.setName("trigger");
                trigger.setGroup("group");
                trigger.setJobKey(rpa.getKey());
                ((SimpleTriggerImpl) trigger).setRepeatInterval(interval);
                ((SimpleTriggerImpl) trigger).setRepeatCount(1000000);
                ((SimpleTriggerImpl) trigger).setStartTime(now);
            }
            
            scheduler.scheduleJob(rpa, trigger);
            
            logger.info("quartz start!");

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }

    }

    /**
     * stop quartz job
     */
    public void stopQuartz() {

        Scheduler scheduler = null;

        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            if (scheduler != null) scheduler.shutdown(false);
            logger.info("quartz stop");

        } catch (SchedulerException e) {
            logger.error("quartz fail!", e);
        }

    }

}