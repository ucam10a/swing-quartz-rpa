package com.yung.app;

import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public interface RunObject {

    public void run(String[] args, Properties prop, JScrollPane logScrollPane, JTextArea log) throws Exception;
    
}
