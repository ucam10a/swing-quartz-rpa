package com.yung.app.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

public abstract class AbstractJob implements Job {

    public static final String NEW_LINE = System.getProperty("line.separator");
    
    public static void logMessage(JobExecutionContext context, String message) {
        JScrollPane scrollPane = (JScrollPane) context.getJobDetail().getJobDataMap().get("logScrollPane");
        JTextArea log = (JTextArea) context.getJobDetail().getJobDataMap().get("log");
        logMessage(scrollPane, log, message);
    }
    
    public static void logMessage(JScrollPane scrollPane, JTextArea log, String message) {
        JScrollBar vertical = scrollPane.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");
        log.append(sdf.format(new Date()) + " " + message + NEW_LINE);
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
    
}
