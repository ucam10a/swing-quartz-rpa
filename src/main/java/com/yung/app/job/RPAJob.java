package com.yung.app.job;

import java.io.File;
import java.math.BigDecimal;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.yung.tool.AppLogger;
import com.yung.tool.FileUtil;
import com.yung.tool.TraceTool;
import com.yung.web.test.App;
import com.yung.web.test.AppConfig;
import com.yung.web.test.RPACommandClient;
import com.yung.web.test.RPAParameter;

public class RPAJob extends AbstractJob {

    private static final AppLogger log = AppLogger.getLogger(RPAJob.class);
    private static final TraceTool traceTool = TraceTool.getTraceTool(log, true);
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        
        try {
            logMessage(context, "start RPA job");
            
            String logNo = System.currentTimeMillis() + "";
            Properties prop = (Properties) context.getJobDetail().getJobDataMap().get("prop");
            prop.put("logNo", logNo);
            String rpaJarPath = prop.getProperty("rpa.jar.path");
            if (rpaJarPath == null) {
                throw new RuntimeException("Please define 'rpa.jar.path' in config.properties");
            }
            
            final File jarFile = FileUtil.getFile(rpaJarPath);
            if (!jarFile.exists()) {
                throw new RuntimeException("jar: " + rpaJarPath + " not found");
            }
            logMessage(context, "logs location: " + jarFile.getParent() + "/logs");
            
            String rpaName = getRpaName(jarFile);
            final AppConfig config = new AppConfig();
            config.setHeadless(Boolean.valueOf(prop.getProperty("rpa.headless")));
            config.setRpaName(rpaName);
            config.setComplexScenarioClsName(prop.getProperty("rpa.scenario"));
            config.setTestParamClsName(prop.getProperty("rpa.test.value"));
            String maxWaitSec = prop.getProperty("rpa.maxWaitSec");
            if (maxWaitSec != null && !"".equals(maxWaitSec) && isNumeric(maxWaitSec)) {
                config.setMaxWaitSec(new BigDecimal(maxWaitSec).intValue());
            }
            String remoteUrl = prop.getProperty("rpa.remoteUrl");
            if (remoteUrl != null && !"".equals(remoteUrl)) {
                config.setRemoteUrl(remoteUrl);
            }
            String proxy = prop.getProperty("rpa.proxy");
            if (proxy != null && !"".equals(proxy)) {
                config.setProxy(proxy);
            }
            traceTool.reflectTrace("runTest", "config", config);
            
            final RPAParameter parameter = new RPAParameter();
            synchronized(RPACommandClient.getPidMap()) {
                
                traceTool.reflectTrace("runTest", "pidMap", RPACommandClient.getPidMap());
                
                String driverLogs = rpaJarPath + "/";
                traceTool.reflectTrace("runTest", "driverLogs", driverLogs);
                String rpaDirPath = App.getRPADir(driverLogs, rpaName);
                traceTool.reflectTrace("runTest", "rpaDirPath", rpaDirPath);
                File rpaDir = FileUtil.getFile(rpaDirPath);
                int pid = findPid(RPACommandClient.getPidMap(), jarFile.getAbsolutePath());
                log.info("pid: " + pid + ", for " + jarFile.getAbsolutePath());
                if (pid > 0) {
                    logMessage(context, "RPA jar:" + jarFile.getAbsolutePath() + " is running!" + RPACommandClient.LINE_SEPARATOR);
                    return;
                } else {
                    // health check
                    if (rpaDir.exists()) {
                        File[] logDirs = rpaDir.listFiles();
                        if (logDirs != null && logDirs.length > 0) {
                            for (File logDir : logDirs) {
                                File tempLog = FileUtil.getFile(logDir.getAbsolutePath() + "/" + App.TEMP_LOG_NAME);
                                File logFile = RPACommandClient.getFileWithPrefix(logDir, App.SERVER_APP_NAME);
                                if (tempLog.exists()) {
                                    // pid not exist, then delete temp log folder
                                    if (logFile != null && logFile.exists()) {
                                        // delete tempLog
                                        FileUtil.delete(tempLog.getAbsolutePath());
                                    } else {
                                        // delete folder
                                        FileUtil.delete(tempLog.getParent());
                                    }
                                }
                            }
                        }
                    }
                }
                
                final String runId = logNo;
                ExecutorService executorService = Executors.newScheduledThreadPool(1);
                executorService.execute(new Runnable() {
                    public void run() {
                        try {
                            traceTool.trace("start to RPACommandClient.exeRPACommand ...");
                            String msg = RPACommandClient.exeRPACommand(this, parameter, jarFile, runId, config, "en");
                            log.info(msg);
                            logMessage(context, msg);
                            prop.put("pid", "");
                            logMessage(context, "RPA job finish");
                        } catch (Exception e) {
                            log.error(e.toString(), e);
                        }
                    }
                });
                executorService.shutdown();
                // wait at most 20 seconds for pid
                for (int i = 0; i < 20; i++) {
                    Thread.sleep(1000);
                    int processId = parameter.getProcessId();
                    if (processId != 0 && RPACommandClient.getPidMap().containsKey(processId)) {
                        prop.put("pid", processId + "");
                        break;
                    }
                }
                
            }
        } catch (Exception e) {
            logMessage(context, "RPA job fail due to " + e.toString());
            throw new RuntimeException(e);
        }
        
    }
    
    private int findPid(ConcurrentHashMap<Integer, RPAParameter> pidMap, String jarFilePath) {
        for (Entry<Integer, RPAParameter> entry : pidMap.entrySet()) {
            if (entry.getValue().getJarFilePath().equals(jarFilePath)) {
                return entry.getKey();
            }
        }
        return -1;
    }
    
    private String getRpaName(File jarFile) {
        String jarName = jarFile.getName();
        int idx = jarName.lastIndexOf(".");
        return jarName.substring(0, idx);
    }
    
}
